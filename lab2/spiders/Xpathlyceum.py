import scrapy
from lab2.items import FacultyItem, NewsItem


class lyceumSpider(scrapy.Spider):
    name = "Xpathlyceum"
    allowed_domains = ["ilnlicey.ucoz.ua"]
    start_urls = ["http://ilnlicey.ucoz.ua/"]

    def parse(self, response):
        
        month_list = response.xpath('//ul[@class="archUl"]/li')

        for li in month_list:
           
            month_name = li.xpath('a/text()').get().strip()
            month_link = response.urljoin(li.xpath('a/@href').get())

            yield FacultyItem(
                name=month_name,
                url=month_link,
            )


            yield response.follow(month_link, callback=self.parse_news)

    def parse_news(self, response):
        
        news_list = response.xpath('//div[@class="uz"]')

        for news_item in news_list:
            
            title = news_item.xpath('.//a[@class="archiveEntryTitleLink"]/text()').get().strip() or "No title found"

            yield NewsItem(
                title=title,
            )
