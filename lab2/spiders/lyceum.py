import scrapy
from bs4 import BeautifulSoup
from lab2.items import FacultyItem,  NewsItem


class lyceumSpider(scrapy.Spider):
  
    name = "lyceum"
    allowed_domains = ["ilnlicey.ucoz.ua"]
    
    start_urls = ["http://ilnlicey.ucoz.ua/"]

    def parse(self, response):
        soup = BeautifulSoup(response.body, "html.parser")
        
        month_list = soup.find(class_="archUl")
        month_links = []

        for li in month_list.find_all("li"):
            a = li.find("a")
            month_name = a.find(text=True, recursive=False)
            month_link = f"http://ilnlicey.ucoz.ua{a.get('href')}"
            month_links.append(month_link)
            
            yield FacultyItem(
                name=month_name,
                url=month_link,
            )

           
            yield scrapy.Request(month_link, callback=self.parse_news)

    def parse_news(self, response):
        soup = BeautifulSoup(response.body, "html.parser")

        news_list = soup.find_all(class_="uz")

        for news_item in news_list:
            
            title_element = news_item.find(class_="archiveEntryTitleLink")
            if title_element:
                title = title_element.text.strip()
            else:
                title = "No title found"

            yield NewsItem(
                title=title,
            )