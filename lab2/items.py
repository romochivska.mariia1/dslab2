
import scrapy


class FacultyItem(scrapy.Item):
    name = scrapy.Field()
    url = scrapy.Field()


class StaffItem(scrapy.Item):
    name = scrapy.Field()
    department = scrapy.Field()

class NewsItem(scrapy.Item):
    title= scrapy.Field()